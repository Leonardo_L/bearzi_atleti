/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 21 2018
 *  File : Utils.js
 *******************************************/

 class Utils {
     static randRange(min,max) {
         var value;
         var rand = Math.random();
         var delta = max-min;
         var value = min+Math.round(delta*rand);

         return value;
     }
 }