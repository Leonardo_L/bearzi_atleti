/******************************************
 *  Author : Author   
 *  Created On : Sat Nov 17 2018
 *  File : Gara.js
 *******************************************/
class Gara {

    constructor(distanza) {
        // istanzio le proprietà d'istanza
        this.gara_vo = new GaraVO();
        this.gara_vo.distanza = distanza;
        this.finita = false;
        this.callbackUpdate = null;
        this.callbackFineGara = null;
        this._interval_gara = null;
        this._ts_partenza = null;
        this._poolAtleti = [
            {nome:"Mario Rossi", sesso:"Maschio"},
            {nome:"Luigi Bianchi", sesso:"Maschio"},
            {nome:"Alberto Romani", sesso:"Maschio"},
            {nome:"Giovanni Petta", sesso:"Maschio"},
            {nome:"Fausto Sgobba", sesso:"Maschio"},
            {nome:"Marco Biondi", sesso:"Maschio"},
            {nome:"Luigi Ancona", sesso:"Maschio"},
            {nome:"Guido Rigoni", sesso:"Maschio"},
            {nome:"Mara Bianconi", sesso:"Femmina"},
            {nome:"Lara Rossi", sesso:"Femmina"},
            {nome:"Anna Tatangelo", sesso:"Femmina"},
            {nome:"Elisa Gianni", sesso:"Femmina"},
            {nome:"Sara Bianchini", sesso:"Femmina"},
            {nome:"Erika Botto", sesso:"Femmina"},
            {nome:"Amanda Lear", sesso:"Femmina"},
            {nome:"Elena Lorcia", sesso:"Femmina"},
            {nome:"Anna Togni", sesso:"Femmina"},
            ]
    }

    /**
     * crea un array di atletaVO
     * e la salva in GaraVO
     * @param {Number} num_atleti 
     * @returns {Atleta[]}
     */
    generaAtleti(num_atleti) {
        var arr_atleti = [];
        //creiamo il callback fuori da lloop in modo da generare una sola funzione
        var callbackArrivato = ()=>this._arrivato();
        // creiamo una copia dell'array
        var _poolAtleti =JSON.parse(JSON.stringify(this._poolAtleti));
        for (var i=0;i<num_atleti;i++) {
            // calcoliamo la posizione random nell'array da cui prendere i dati
            var posizioneCasuale = Utils.randRange(0,_poolAtleti.length-1)
            // recuperiamo i dati
            var infoAtleta = _poolAtleti.splice(posizioneCasuale,1)[0];
            // calcoliamo l'età random        
            var eta = Utils.randRange(15,60);
            // creiamo l'atleta
            var atleta = new Atleta(infoAtleta.nome,eta,infoAtleta.sesso,this.gara_vo.distanza,callbackArrivato);
            arr_atleti.push(atleta);
        }
        console.log (arr_atleti);
        //aggiorno la VO
        this.gara_vo.atleti = arr_atleti;
        return arr_atleti;
    }

    /**
     * callback di chiamata quando un atleta arriva
     * al traguardo
     */
    _arrivato() {
        console.log("arrivato");
        // salvo gli atleti in una variabile locale
        var atleti =  this.gara_vo.atleti;
        var atletiArrivati = 0
        for (var i=0; i<atleti.length; i++) {
            var atleta = atleti[i];
            if (atleta.arrivato) atletiArrivati++;
        }
        //
        if (atletiArrivati>=atleti.length) {
            this._finegara();
        }
    }

    _finegara() {
        console.log("fine gara");
        window.cancelAnimationFrame(this._interval_gara);
        this._interval_gara = null;
        this.finita = true;
    }

    partenza(callbackUpdate,callbackFineGara) {
        this.callbackFineGara = callbackFineGara;
        this.callbackUpdate = callbackUpdate;
        // salvo in una varlabile d'istanza im momento corrente
        this.ts_partenza = Date.now();
        console.log(this.ts_partenza)
        //
        // richiamo una funzione temporizzata da eseguire finché la gara non finisce
        this._interval_gara = window.requestAnimationFrame(()=>this._avanzamentoTempo());
    }

    _avanzamentoTempo() {
        // calcolo quanti secondi sono passati dall'inizio della gara
        var ts_now = Date.now();
        var ms_trascorsi = ts_now-this.ts_partenza;
        var tempoTrascorso = ms_trascorsi/1000;
        console.log("avanzamento "+tempoTrascorso)
        // TODO: andrebbe calcolato
        // lop tra gli atleti per vedere dove sono arrivati
        for (var i=0;i<this.gara_vo.atleti.length;i++) {
            var atleta = this.gara_vo.atleti[i];
            atleta.corri(tempoTrascorso);
        }
        // richiamo il callback per segnalare che sono cambiati i dati
        this.callbackUpdate();
        //
        if(!this.finita) this._interval_gara = window.requestAnimationFrame(()=>this._avanzamentoTempo());
    }

}